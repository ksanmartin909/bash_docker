FROM mediawiki

ENV WIKI_EXTERNAL_PORT=''
ENV WIKI_INTERNAL_PORT=''
ENV WIKI_NAME=''
ENV WIKI_USER=''
ENV WIKI_DB_SERVICE_NAME=''
ENV WIKI_ADMIN_PASSWORD=''
ENV DB_PORT=''
ENV DB_NAME=''
ENV DB_PASSWORD=''
ENV DB_ROOT_USER=''
ENV DB_TYPE=''
ENV DB_SERVER=''

RUN apt-get -y update
RUN apt-get -y install nano
RUN apt-get -y install default-mysql-client


COPY ./wikiInit.sh  ./

ENTRYPOINT [ "bash", "./wikiInit.sh" ] 
